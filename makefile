####################################################################
# Makefile to build libsimplefileadapter.so
####################################################################
ARCH = x64Darwin17clang9.0
c_cc = clang
c_ld = clang
ifeq ($(DEBUG),1)
c_cc_flags = -m64 -g
else
c_cc_flags = -m64
endif
c_ld_flags = -m64 -Wno-return-type-c-linkage
syslibs = -ldl -lm -lpthread

ifndef NDDSHOME
NDDSHOME := "/Applications/rti_connext_dds-5.3.1"
endif

DEFINES_ARCH_SPECIFIC = -DRTI_UNIX -DRTI_DARWIN -DRTI_DARWIN16 -DRTI_64BIT 
DEFINES = $(DEFINES_ARCH_SPECIFIC)
INCLUDES = -I. -I$(NDDSHOME)/include -I$(NDDSHOME)/include/ndds

LIBS = -L$(NDDSHOME)/lib/$(ARCH) \
-lnddsc -lnddscore -lrtirsinfrastructure $(syslibs) $(extralibs)

COMMONSOURCES = SimpleFileAdapter.c ShapeTypeParser.c
SHAREDLIB = lib/$(ARCH)/libsimplefileadapter.dylib
DIRECTORIES = lib.dir lib/$(ARCH).dir objs.dir objs/$(ARCH).dir
COMMONOBJS = $(COMMONSOURCES:%.c=objs/$(ARCH)/%.o)

$(ARCH) : $(DIRECTORIES) $(COMMONOBJS) $(SHAREDLIB)

$(SHAREDLIB) : $(COMMONOBJS)
	$(c_cc) $(c_ld_flags) -shared -o $@ $^ $(LIBS)

objs/$(ARCH)/%.o : %.c
	$(c_cc) $(c_cc_flags) -o $@ $(DEFINES) $(INCLUDES) -c $<

# Here is how we create those subdirectories automatically.
%.dir :
	@echo "Checking directory $*"
	@if [ ! -d $* ]; then \
			echo "Making directory $*"; \
			mkdir -p $* ; \
	fi;
 clean:
	@rm -rf ./objs
	@rm -rf ./lib