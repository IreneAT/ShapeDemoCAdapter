#include "ndds/ndds_c.h"
#include "ShapeTypeParser.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/*This internal method splits a string into a array of strings, using the a_delimit char as the token*/
char** ShapeTypeParser_strSplit(char* a_str, const char a_delim)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token != NULL)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(NULL, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = NULL;
    }

    return result;
}


void ShapeTypeParser_deserialize(char* serializedData, DDS_DynamicData* sample){
    if (sample==NULL) return;
    char ** tokens = ShapeTypeParser_strSplit(serializedData,',');
    if (tokens)
    {
        int i;
        for (i = 0; *(tokens + i); i++)
        {
            char** field = ShapeTypeParser_strSplit(*(tokens+i),'=');
            char* fieldName = *field;
            
            if (strcmp(*field,"color")==0){
                char fieldValue[strlen(*(field+1))];
                strcpy(fieldValue,*(field+1));
                DDS_ReturnCode_t retCode = DDS_DynamicData_set_string(
                    sample, fieldName, 
                    DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, 
                    fieldValue);
                if (retCode != DDS_RETCODE_OK) {
                    printf("Error assigning the field %s \n", fieldName);
                } 
            } else {
                DDS_Long fieldValue = atol(*(field+1));
                DDS_ReturnCode_t retCode = DDS_DynamicData_set_long(
                    sample, fieldName, 
                    DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, 
                    fieldValue);
                if (retCode != DDS_RETCODE_OK) {
                    printf("Error assigning the field =%s, %i \n", fieldName, fieldValue);
                } 
            }  
            free(*(tokens + i));
        }
        free(tokens);
    }
    fflush(stdout);

}

void ShapeTypeParser_serialize(DDS_DynamicData* sample, char* serializedData, unsigned int strsize){
    DDS_Long x;
    DDS_Long y;
    DDS_Long shapesize;
    DDS_UnsignedLong size = 50;
    char* color = NULL;
    DDS_DynamicData_get_string(sample, &color, &size, "color",  DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED);
    DDS_DynamicData_get_long(sample, &x, "x",  DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED);
    DDS_DynamicData_get_long(sample, &y, "y",  DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED);
    DDS_DynamicData_get_long(sample, &shapesize, "shapesize",  DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED);

    snprintf(serializedData,strsize,"color=%s,x=%i,y=%i,shapesize=%i%c",color,x,y,shapesize,'\0');
}