Routing Service C Adapter Example for ShapeType
===============================================

This adapter can:

* write ShapeType received form DDS to a file.
* read ShapeType from a file and send it via DDS.

Compile the project
-------------------

Be sure you have been set $NDDSHOME

The make file is prepared to compile for MacOX (x64Darwin17clang9.0 arch). You could use it to compile in other UNIX based system with minor changes.

$ make -f makefile

Clean the project
-----------------

$ make clean

Run Routing Service
-------------------

Reading from a file and write to a file:

$ $NNDSHOME/bin/rtiroutingservice -cfgFile simple_file_adapter.xml -cfgName file_to_file

Receiving from DDS and writing to a file:

$ $NNDSHOME/bin/rtiroutingservice -cfgFile simple_file_adapter.xml -cfgName file_to_dds

Reading from a file and sending via DDS:

$ $NNDSHOME/bin/rtiroutingservice -cfgFile simple_file_adapter.xml -cfgName file_to_dds

