#include "ndds/ndds_c.h"

/*This method parses a ShapeType sample (as DynamicData) into a string*/
void ShapeTypeParser_deserialize(char* serializedData, DDS_DynamicData* sample);
/*This method parses a string into a ShapeType sample (as DynamicData) */
void ShapeTypeParser_serialize(DDS_DynamicData* sample, char* serializedData, unsigned int strsize);